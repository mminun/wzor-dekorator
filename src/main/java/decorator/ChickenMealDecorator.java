package decorator;
import decorator.meal.Meal;
public class ChickenMealDecorator extends MealDecorator {
    public ChickenMealDecorator(Meal decoratedMeal) {
        super(decoratedMeal);
    }

    public void addChicken() {
        System.out.println("dodano kure");
    }

    @Override
    public void prepareMeal() {
        addChicken();
        meal.prepareMeal();
    }
}
