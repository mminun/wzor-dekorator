package decorator;

import decorator.meal.Meal;
import decorator.meal.RiceMeal;

public class Main {

    public static void main(String[] args) {
        System.out.println("nowe danie - ryz");
        Meal jedzonko = new RiceMeal();
        jedzonko.prepareMeal();

        System.out.println("dodajemy kurczaka");
        Meal kurczakZRyzem = new ChickenMealDecorator(jedzonko);
        kurczakZRyzem.prepareMeal();
        System.out.println("dodajemy do tego sos");
        Meal kurczakZRyzemISosem = new SauceMealDecorator(kurczakZRyzem);
        kurczakZRyzemISosem.prepareMeal();
    }
}
