package decorator.meal;

public abstract class Meal {

    public void prepareMeal() {
        System.out.println("przygotowuje danie");
    }
}
