package decorator;
import decorator.meal.Meal;
public class SauceMealDecorator extends MealDecorator {
    public SauceMealDecorator(Meal meal) {
        super(meal);
        addSauce();
    }

    public void addSauce() {
        System.out.println("dodano sos");
    }
}
